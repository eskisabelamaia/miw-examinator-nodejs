var express = require('express');
var router = express.Router();

var Exams = require('../models/exams');
var Questions = require('../models/questions');
var Users = require('../models/users');
var UsersExams = require('../models/usersexams');

var xmlify = require('xmlify');

/* GET home page. */
router.get('/', function(req, res) {
    var invalid = false;
	var exist = false;
	req.session.user = '';
	req.session.logged = false;
	res.render('examinator', {
	  title: 'Examinator',
      "invalid" : invalid,
	  "exist" : exist
    });
});

router.post('/login', function(req, res) {
	Users.findOne({username: req.body.username},function(err, users){
		if (err) {
		  res.redirect('/');
		} else {
		  if(users.password === req.body.password){
		    req.session.user = req.body.username;
			req.session.logged = true;
			res.redirect('/question');
		  }else{
		    var invalid = true;
		    res.render('examinator', {
              title: 'Examinator',
			  "invalid" : invalid
            });
		  }
		}
    });
});

router.post('/register', function(req, res) {
	var exist = false;
	Users.findOne({username: req.body.username},function(err, users){
		if (users==null) {
		  var user = new Users();
		  user.username = req.body.username;
		  user.password = req.body.password;
	
    	  user.save( function(err, user){
		    if (err) {
			  res.send(err);
		    } else {
			  req.session.user = req.body.username;
			  req.session.logged = true;
			  res.redirect('/question');
		    }
	      });
		} else {
		  var exist = true;
		  res.render('examinator', {
            title: 'Examinator',
			"exist" : exist
          });
		}
    });
});

router.get('/question', logged, function(req, res) {
	Questions.find({},function(err, questions){
		if (err) {
		  res.send(err);
		} else {
		  res.render('question', {
            "questions" : questions,
			"user" : req.session.user
          });
		}
    });
});

router.get('/answer/:qid', logged, function(req, res) {
	Questions.findById(req.params.qid, function(err, question) {
		if (err) {
		  res.send(err);
		} else {
		  res.render('answer', {
            "question" : question,
			"user" : req.session.user
          });
		}
    });
});

router.post('/addquestion', logged, function(req, res) {

	var q = new Questions();
	q.text = req.body.qtext;
    q.type = req.body.type;
    q.language = req.body.language;
	q.voteup = 0;
	q.votedown = 0;
	
	q.save( function(err){
		if (err) {
		  res.send(err);
		} else {
		  res.redirect('/question');
		}
	});
});

router.get('/question/:q_id/votedown', logged, function(req, res) {
	Questions.findById(req.params.q_id, function(err, q) {
    if (err) {
      res.send(err);
    } else {
      q.votedown--;
      q.markModified("votedown");
      q.save(function(err) {
        if (err) {
          res.send(err);
        } else {
          res.redirect('/question');
        }
      });
    }
  });
});

router.get('/question/:q_id/addvote', logged, function(req, res) {
	Questions.findById(req.params.q_id, function(err, q) {
    if (err) {
      res.send(err);
    } else {
      q.voteup++;
      q.markModified("voteup");
      q.save(function(err) {
        if (err) {
          res.send(err);
        } else {
          res.redirect('/question');
        }
      });
    }
  });
});

router.post('/question/addanswer', logged, function(req, res) {
	Questions.findById(req.body.qid, function(err, q) {
		if (err) {
		  res.send(err);
		} else {
		  q.answers.push({ score: req.body.ascore, text: req.body.atext });
		  q.save(function(err) {
		  if (err) {
			res.send(err);
          } else {
			res.redirect('/answer/'+req.body.qid);
		  }
		});
    }
  });
});

router.post('/question/edit', logged, function(req, res) {
	Questions.findById(req.body.qid, function(err, q) {
		if (err) {
		  res.send(err);
		} else {
		  q.text = req.body.qtext;
		  q.type = req.body.qtype;
		  q.language = req.body.qlanguage;

		  q.save(function(err) {
		  if (err) {
			res.send(err);
          } else {
			res.redirect('/question');
		  }
		});
    }
  });
});

router.post('/answer/edit', logged, function(req, res) {
	Questions.findById(req.body.qid, function(err, q) {
		if (err) {
		  res.send(err);
		} else {
		  for (i = 0; i < q.answers.length; i++) {
			if(q.answers[i]._id.equals(req.body.aid)){
			  q.answers[i].score = req.body.ascore;
		      q.answers[i].text = req.body.atext;
			}
		  }
		  q.markModified("answers");
		  q.save(function(err) {
		    if (err) {
			  res.send(err);
            } else {
			  res.redirect('/answer/'+req.body.qid);
		    }
		  });
    }
  });
});

router.get('/question/:q_id/delete', logged, function(req, res) {
  Questions.remove({ _id: req.params.q_id }, function(err) {
    if (err) {
      res.send(err);
    } else {
      res.redirect('/question');
    }
  });
});

router.get('/answer/:question_id/:a_id/delete', logged, function(req, res) {
	Questions.findById(req.params.question_id, function(err, q) {
		if (err) {
		  res.send(err);
		} else {
		  for (i = 0; i < q.answers.length; i++) {
			if(q.answers[i]._id.equals(req.params.a_id)){
			  q.answers.splice(i, 1);
			}
		  }
		  q.markModified("answers");
		  q.save(function(err) {
		    if (err) {
			  res.send(err);
            } else {
			  res.redirect('/answer/'+req.params.question_id);
		    }
		  });
    }
  });
});

router.get('/exams', logged, function(req, res) {
	Exams.find({},function(err, exams){
		if (err) {
		  res.send(err);
		} else {
		  res.render('exams', {
            "exams" : exams,
			"user" : req.session.user
          });
		}
    });
});

router.post('/exam/create', logged, function(req, res) {	
	Questions.find().limit(req.body.qNum).exec(function (err, q) {
	  if (err) {
	    res.send(err);
	  } else {
		var exam = new Exams();
		exam.questQuantity = req.body.qNum;
		for(var i=0; i<q.length; i++) {
			exam.questions.push(q[i]);
		}
		exam.save(function(err) {
		  if (err) {
	        res.send(err);
          } else {
	        res.redirect('/exams');
		  }
		});
	  }
    });
})

router.get('/exam/:e_id', logged, function(req, res) {
	if(req.query.examformat=='HTML'){
		Exams.findById(req.params.e_id,function(err, exam){
			if (err) {
				res.send(err);
			} else {
				res.set('Content-Type', 'text/html');
				res.render('exam', {
					"exam" : exam,
					"user" : req.session.user
				});
			}
		});
	}
	if(req.query.examformat=='JSON'){
	
		Exams.find({_id : req.params.e_id})
			.select('_id questions._id questions.type questions.text questions.answers')
			.exec(function(err, exam) {
            if (err) {
              res.send(err);
            } else {
			  res.set('Content-Type', 'text/json');
			  var examjson = JSON.stringify(exam);
			  res.json(examjson);
            }
        });
	}
	if(req.query.examformat=='XML'){
		Exams.find({_id : req.params.e_id})
			.select('_id questions._id questions.type questions.text questions.answers')
			.exec(function(err, exam) {
			if (err) {
				res.send(err);
			} else {
				res.set('Content-Type', 'text/xml');
				var examxml = xmlify(exam);
				res.send(examxml);
			}
		});
	}
});

router.get('/exams/:e_id/delete', logged, function(req, res) {
  Exams.remove({ _id: req.params.e_id }, function(err) {
    if (err) {
      res.send(err);
    } else {
      res.redirect('/exams');
    }
   });
});

router.get('/exam/:exam_id/evaluate', logged, function(req, res) {
	UsersExams.find({examid:req.params.exam_id},function(err, usersexam){
	  if(err) {
	    	Exams.findById(req.params.exam_id,function(err, exam){
			  if (err) {
				res.send(err);
			  } else {
				  var usersexam = new UsersExams();
				  usersexam.examid = req.params.exam_id;
				  usersexam.user = req.session.user;
				  var score = 0;
				  for(var q_id in req.query){
					for(var i=0;i<exam.questions.length;i++){
					  if(exam.questions[i].type=='truefalse') {
						for(var j=0;j<exam.questions[i].answers.length;j++){
						  if(exam.questions[i].answers[j]._id == req.query[q_id]) {
							score += exam.questions[i].answers[j].score;
							console.log(score);
						  }
						}
					  }
					  if(exam.questions[i].type=='multichoice') {
						for(var a_id in req.query[q_id]) {
						  for(var j=0;j<exam.questions[i].answers.length;j++){
							console.log(exam.questions[i].answers[j]);
							if(exam.questions[i].answers[j]._id == req.query[q_id][a_id]) {
							  score += exam.questions[i].answers[j].score;
							  console.log(score);
							}
						  }
						}
					  }
					}
				  }
				  usersexam.score = score;
				  usersexam.save(function(err) {
						if (err) {
						  res.send(err);
						} else {
							res.redirect('/profile');
						}
				  });
			  }
			});
	  }else{
	    Exams.findById(req.params.exam_id,function(err, exam){
		  if (err) {
		    res.send(err);
		  } else {
		    var score = 0;
			for(var q_id in req.query){
			  for(var i=0;i<exam.questions.length;i++){
				if(exam.questions[i].type=='truefalse') {
				  for(var j=0;j<exam.questions[i].answers.length;j++){
					if(exam.questions[i].answers[j]._id == req.query[q_id]) {
					  score += exam.questions[i].answers[j].score;
					}
				  }
				}
				if(exam.questions[i].type=='multichoice') {
				  for(var a_id in req.query[q_id]) {
					for(var j=0;j<exam.questions[i].answers.length;j++){
					  if(exam.questions[i].answers[j]._id == req.query[q_id][a_id]) {
						score += exam.questions[i].answers[j].score;
					  }
					}
				  }
				}
			  }
			}
		  }
		  usersexam[0].score = score;
		  usersexam[0].save(function(err) {
		    if (err) {
		      res.send(err);
		    } else {
			  res.redirect('/profile');
		    }
		  });
		});
	  }
	});
});

router.get('/profile', logged, function(req, res) {
	UsersExams.find({user:req.session.user},function(err, usersexam){
		if (err) {
		  res.send(err);
		} else {
		  res.render('profile', {
            "exams" : usersexam,
			"user" : req.session.user
          });
		}
    });
});

router.get('/profile/:e_id/delete', logged, function(req, res) {
  UsersExams.remove({ _id: req.params.e_id }, function(err) {
    if (err) {
      res.send(err);
    } else {
      res.redirect('/profile');
    }
   });
});

router.get('/exam/:examid', logged, function(req, res) {
	if(req.query.examformat=='HTML'){
		Exams.findById(req.params.examid,function(err, exam){
			if (err) {
				res.send(err);
			} else {
				res.set('Content-Type', 'text/html');
				res.render('exam', {
					"exam" : exam,
					"user" : req.session.user
				});
			}
		});
	}
});

module.exports = router;

function logged(req, res, next) {
  if (req.session.logged==true) {
    return next();
  }else{
    res.redirect('/');
  }
}