$(document).on("click", ".open-EditQuestion", function () {
     $(".modal-body #qid").val( $(this).data('id') );
	 $(".modal-body #qtype").val( $(this).data('type') );
	 $(".modal-body #qlanguage").val( $(this).data('language') );
	 $(".modal-body #qtext").val( $(this).data('text') );
    $('#edit').modal('show');
});
$(document).on("click", ".open-AddAnswers", function () {
     $(".modal-body #qid").val( $(this).data('id') );
    $('#answers').modal('show');
});
$(document).on("click", ".open-EditAnswer", function () {
	 $(".modal-body #qid").val( $(this).data('id') );
     $(".modal-body #aid").val( $(this).data('answerid') );
	 $(".modal-body #ascore").val( $(this).data('score') );
	 $(".modal-body #atext").val( $(this).data('text') );
    $('#editAnswer').modal('show');
});