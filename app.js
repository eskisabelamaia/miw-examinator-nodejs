var express = require('express');
var expressSession = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var i18n = require('i18n-2');
var cloudinary = require('cloudinary');

cloudinary.config({
	cloud_name: 'hkqpunlaq',
	api_key: '724691592299447',
	api_Secret: 'sf7RfrQ013oZotMyLEZaWuFcpNE'
});

/* mongoose.connect('mongodb://localhost:27017/examinatordb');*/
var dbremote = require('./dbremote/db.js');
mongoose.connect(dbremote.url);
var db = mongoose.connection;
db.once('open', function () {
    console.log('Connected to DB!');
  });
db.on('error', console.error.bind(console, 'DB connection error: '));

var routes = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressSession({secret: 'examinatorsecret',
	resave: true,
	saveUninitialized: true}) 
);
i18n.expressBind(app, {
  locales: ['es', 'en','eu'],
  defaultLocale: 'en',
  cookieName: 'locales'
});
app.use(function(req, res, next) {
  req.i18n.setLocaleFromQuery();
  next();
});
app.use(function(req,res,next){
    req.db = db;
    next();
});

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = function sessions (req,res, next) {
	res.locals.session = req.session;
	next();
} 

module.exports = app;
