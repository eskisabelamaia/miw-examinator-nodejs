var mongoose = require('mongoose');
var autoincrement = require('mongoose-auto-increment');
var random = require('mongoose-random');

autoincrement.initialize();

var questionsSchema = mongoose.Schema({
  text: {
    type: String,
    unique: true,
    required: true
  },
  type: {
    type: String,
    enum: ['multichoice', 'truefalse'],
    required: true
  },
  language: {
    type: String,
    enum: ['es', 'en','eu'],
    required: true
  },
  answers: [{
    text: String,
    score: {
      type: Number,
      min: 0,
      max: 10,
      default: 0
    }
  }],
  voteup: [{
    type: Number,
	min: 0,
    default: 0
  }],
  votedown: [{
    type: Number,
	min: 0,
    default: 0
  }],
});

questionsSchema.plugin(autoincrement.plugin, {
    model: 'questions',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});

questionsSchema.plugin(random);

module.exports = mongoose.model('questions', questionsSchema);