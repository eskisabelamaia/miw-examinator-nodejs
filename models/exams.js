var mongoose = require('mongoose');
var autoincrement = require('mongoose-auto-increment');

autoincrement.initialize();

var examsSchema = mongoose.Schema({
  questQuantity: {
    type: Number
  },
  questions: [{
    type: mongoose.Schema.Types.Object,
    ref: 'questions'
  }],
});

examsSchema.plugin(autoincrement.plugin, {
    model: 'exams',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('exams', examsSchema);