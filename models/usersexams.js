var mongoose = require('mongoose');
var autoincrement = require('mongoose-auto-increment');

autoincrement.initialize();

var usersexamsSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.Object,
    ref: 'users'
  },
  examid: [{
    type: Number
  }],
  score: {
    type: Number
  }
});

usersexamsSchema.plugin(autoincrement.plugin, {
    model: 'userexam',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('usersexams', usersexamsSchema);