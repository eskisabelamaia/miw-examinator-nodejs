var mongoose = require('mongoose');
var autoincrement = require('mongoose-auto-increment');

autoincrement.initialize();

var usersSchema = mongoose.Schema({
	username: { type: String, index: { unique: true } },
	password: { type: String} 
});

usersSchema.plugin(autoincrement.plugin, {
    model: 'users',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('users', usersSchema);