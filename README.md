# Examinator

Examinator is an application created to initialize with nodejs and mongodb.

The application complies with the following functionalities:

    - Generate tests in 3 different formats (HTML, JSON and XML)
    - Generate and manage questions
    - Generate and manage answers
    - Rate questions
    - Create and manage exams
    - User authentication
    - Choose the language of the application between Spanish, English and Basque

Screenshot

![Alt text](https://bytebucket.org/eskisabelamaia/miw-examinator-nodejs/raw/4a58225dd0a85898c4dc5d3d55e153e92229b8ff/screenshot.png "Screenshot")